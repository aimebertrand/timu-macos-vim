" Project: Macos Vim Theme
" Color theme with cyan as a dominant color.
" Code boilerplate from nord-vim
" License: MIT

let s:timu_macos_vim_version="1.0.0"
let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:sg0 = ["#222327", "NONE"]
let s:sg1 = ["#202125", 0]
let s:sg2 = ["#2a2a2a", "NONE"]
let s:sg3 = ["#616161", 8]
let s:sg4 = ["#e8e8e8", "NONE"]
let s:sg5 = ["#f4f4f4", 7]
let s:sg6 = ["#ffffff", 15]
let s:sg7 = ["#91f3e7", 14]
let s:sg8 = ["#88c0d0", 6]
let s:sg9 = ["#50a5eb", 4]
let s:sg10 = ["#50a5eb", 12]
let s:sg11 = ["#ec5f5e", 1]
let s:sg12 = ["#46d9ff", 11]
let s:sg13 = ["#f6c844", 3]
let s:sg14 = ["#78b856", 2]
let s:sg15 = ["#e45c9c", 5]

let s:p.normal.left = [ [ s:sg9, s:sg1 ], [ s:sg5, s:sg2 ] ]
let s:p.normal.middle = [ [ s:sg5, s:sg1 ] ]
let s:p.normal.right = [ [ s:sg5, s:sg2 ], [ s:sg5, s:sg2 ] ]
let s:p.normal.warning = [ [ s:sg1, s:sg13 ] ]
let s:p.normal.error = [ [ s:sg1, s:sg11 ] ]

let s:p.inactive.left =  [ [ s:sg1, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.inactive.middle = g:sg_uniform_status_lines == 0 ? [ [ s:sg5, s:sg1 ] ] : [ [ s:sg5, s:sg3 ] ]
let s:p.inactive.right = [ [ s:sg5, s:sg1 ], [ s:sg5, s:sg1 ] ]

let s:p.insert.left = [ [ s:sg11, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.replace.left = [ [ s:sg15, s:sg1 ], [ s:sg5, s:sg1 ] ]
let s:p.visual.left = [ [ s:sg13, s:sg1 ], [ s:sg5, s:sg1 ] ]

let s:p.tabline.left = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.middle = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.right = [ [ s:sg5, s:sg3 ] ]
let s:p.tabline.tabsel = [ [ s:sg1, s:sg8 ] ]

let g:lightline#colorscheme#timu_macos_vim#palette = lightline#colorscheme#flatten(s:p)
